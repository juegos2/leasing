# leasing de items de ludoteca


## precondiciones

- la ludoteca está asociada a un local, para el cual el público paga entrada
- el local está cerrado al publico por la ASPO
- los items de la ludoteca están improdctivos


## objetivo

- satisfacer la demanda de uso de los ítems por parte del público
- satisfacer la necesidad de flujo entrante de dinero por parte de la ludoteca
- aprovechar los ítems improductivos

## propuesta

se propone  implementar un alquiler de los ítems en forma de leasing. 
Por recibir el item el usuario  paga un monto que equivale al costo de compra, pero en lugar de una compra directa, el pago otorga  los siguientes derechos/obligaciones:
- obligacion de uso por un minimo de meses (ejemplo 3 meses) o hasta que reabra el local
- obligacion de **no usar** el juego por un minimo de dias como medida sanitaria
- crédito por una cantidad X de pases para cuando el local reabra  
- si el usuario devuelve el juego, puede pedir otro pagando el costo en plata y, opcionalmente, con su saldo de pases


###  Juego,  estados 

![](README.d/FSM.png)


### Juego, clase


![](README.d/claseJuego.png)


## dudas

- determinar el valor en ARS de los Pases de entrad al local
- certificar la desinfeccion / periodo de retension entre clientes
